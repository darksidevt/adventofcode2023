def read_maps_from_file(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()

    seed_numbers = list(map(int, lines[0].split()[1:]))
    maps_list = []

    for i, line in enumerate(lines):
        if line.startswith("seed-to-soil map:"):
            seed_to_soil_map = [tuple(map(int, map_str.split())) for map_str in lines[i + 1:]]
            maps_list.append(seed_to_soil_map)
        elif line.startswith("soil-to-fertilizer map:"):
            soil_to_fertilizer_map = [tuple(map(int, map_str.split())) for map_str in lines[i + 1:]]
            maps_list.append(soil_to_fertilizer_map)
        elif line.startswith("fertilizer-to-water map:"):
            fertilizer_to_water_map = [tuple(map(int, map_str.split())) for map_str in lines[i + 1:]]
            maps_list.append(fertilizer_to_water_map)
        elif line.startswith("water-to-light map:"):
            water_to_light_map = [tuple(map(int, map_str.split())) for map_str in lines[i + 1:]]
            maps_list.append(water_to_light_map)
        elif line.startswith("light-to-temperature map:"):
            light_to_temperature_map = [tuple(map(int, map_str.split())) for map_str in lines[i + 1:]]
            maps_list.append(light_to_temperature_map)
        elif line.startswith("temperature-to-humidity map:"):
            temperature_to_humidity_map = [tuple(map(int, map_str.split())) for map_str in lines[i + 1:]]
            maps_list.append(temperature_to_humidity_map)
        elif line.startswith("humidity-to-location map:"):
            humidity_to_location_map = [tuple(map(int, map_str.split())) for map_str in lines[i + 1:]]
            maps_list.append(humidity_to_location_map)

    return seed_numbers, maps_list

def convert_number(source, maps):
    for dest_start, source_start, length in maps:
        if source_start <= source < source_start + length:
            return dest_start + (source - source_start)
    return source  # If the source number isn't mapped, return itself

def find_lowest_location(seed_numbers, maps_list):
    current_numbers = seed_numbers

    for maps in maps_list:
        next_numbers = []

        for source_number in current_numbers:
            converted_number = convert_number(source_number, maps)
            next_numbers.append(converted_number)

        current_numbers = next_numbers

    return min(current_numbers)

if __name__ == "__main__":
    file_path = "example1.txt"
    seed_numbers, maps_list = read_maps_from_file(file_path)

    result = find_lowest_location(seed_numbers, maps_list)
    print("The lowest location number is:", result)
