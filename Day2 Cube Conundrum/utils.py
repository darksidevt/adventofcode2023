def open_file(file_name):
    with open(file_name, 'r') as f:
        lines = f.readlines()
        lines = [line.rstrip('\n') for line in lines]
        return lines