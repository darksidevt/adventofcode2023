import utils
from icecream import ic
import re

FILE_NAME = 'input.txt'

def redCubeCount(line):
    reg = r'(\d+) red'
    match = re.search(reg, line)
    if match:
        return int(match.group(1))
    else:
        return 0
def greenCubeCount(line):
    reg = r'(\d+) green'
    match = re.search(reg, line)
    if match:
        return int(match.group(1))
    else:
        return 0
def blueCubeCount(line):
    reg = r'(\d+) blue'
    match = re.search(reg, line)
    if match:
        return int(match.group(1))
    else:
        return 0
def extractGameResults(line):
    reg = r'Game \d+:'
    line = re.sub(reg, '', line)
    list_of_games = line.split(';')
    allgamesresult = []
    for game_results in list_of_games:
        allgamesresult.append({'red': redCubeCount(game_results), 'green': greenCubeCount(game_results), 'blue': blueCubeCount(game_results)})
    return allgamesresult

def extractGameNumber(line):
    reg = r'Game (\d+)'
    results = re.search(reg, line)
    return results.group(1)

def jsonifyGames(lines):
    json_format = {}
    for line in lines:
        json_format[extractGameNumber(line)] = extractGameResults(line)
    return json_format

def isPossible(game,max_cube_count):
    for color, value in game.items():
        if value > max_cube_count[color]:
            return False
    return True
lines = utils.open_file(FILE_NAME)
max_cube_count = {'red': 12, 'green': 13, 'blue': 14}
games_json = jsonifyGames(lines)
temp_json = games_json.copy()
rejection = []
for game, results in games_json.items():
    for handfull in results:
        possible = isPossible(handfull, max_cube_count)
        if not possible:
            try:
                temp_json.pop(game)
            except:
                rejection.append(game)  
games_json = temp_json.copy()
total = 0
for game in games_json:
    total += int(game)
ic(total)