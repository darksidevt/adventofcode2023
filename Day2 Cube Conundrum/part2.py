import utils
from icecream import ic
import re

FILE_NAME = 'input.txt'

def redCubeCount(line):
    reg = r'(\d+) red'
    match = re.search(reg, line)
    if match:
        return int(match.group(1))
    else:
        return 0
def greenCubeCount(line):
    reg = r'(\d+) green'
    match = re.search(reg, line)
    if match:
        return int(match.group(1))
    else:
        return 0
def blueCubeCount(line):
    reg = r'(\d+) blue'
    match = re.search(reg, line)
    if match:
        return int(match.group(1))
    else:
        return 0
def extractGameResults(line):
    reg = r'Game \d+:'
    line = re.sub(reg, '', line)
    list_of_games = line.split(';')
    allgamesresult = []
    for game_results in list_of_games:
        allgamesresult.append({'red': redCubeCount(game_results), 'green': greenCubeCount(game_results), 'blue': blueCubeCount(game_results)})
    return allgamesresult

def extractGameNumber(line):
    reg = r'Game (\d+)'
    results = re.search(reg, line)
    return results.group(1)

def jsonifyGames(lines):
    json_format = {}
    for line in lines:
        json_format[extractGameNumber(line)] = extractGameResults(line)
    return json_format

def isPossible(game,max_cube_count):
    for color, value in game.items():
        if value > max_cube_count[color]:
            return False
    return True
def maxRedCubes(game):
    list_of_red = []
    for handfull in game:
        for color, value in handfull.items():
            if color == 'red':
                list_of_red.append(value)
    return max(list_of_red)
def maxGreenCubes(game):
    list_of_green = []
    for handfull in game:
        for color, value in handfull.items():
            if color == 'green':
                list_of_green.append(value)
    return max(list_of_green)
def maxBlueCubes(game):
    list_of_blue = []
    for handfull in game:
        for color, value in handfull.items():
            if color == 'blue':
                list_of_blue.append(value)
    return max(list_of_blue)
    
lines = utils.open_file(FILE_NAME)
games_json = jsonifyGames(lines)
# Create a json with the largest number of cubes of each color
largest_json = {}
for game, results in games_json.items():
    largest_json[game]={'red': maxRedCubes(results), 'green': maxGreenCubes(results), 'blue': maxBlueCubes(results)}
# Create a json with the multipied sum of the cubes of each color
sum_of_colors = {}
for game, results in largest_json.items():
    total = 1
    for color, value in results.items():
        total = total*value
    sum_of_colors[game] = total
# Add all the sums of the games   
sum_of_games = 0
for game, total in sum_of_colors.items():
    sum_of_games += int(total)
ic(sum_of_games)