from icecream import ic
import utils
import re
import math
FILE = 'input.txt'
data = utils.open_file(FILE)
ic(data)
winning_list = []
for line in data:
    reg = r'Card \d+:'
    line = re.sub(reg, '', line)
    line = line.replace('  ', ' ')
    line = line.split('|')
    line[0] = line[0].strip()
    line[0] = line[0].split(' ')
    line[1] = line[1].strip()
    line[1] = line[1].split(' ')
    temp_list = []
    for winning_number in line[0]:
        if winning_number in line[1]:
            temp_list.append(winning_number)
    winning_list.append(temp_list)
ic(winning_list)
solution = 0
for card in winning_list:
    if card != []:
        solution += (math.pow(2,len(card)-1))
# ic(math.pow(2,len(winning_list[3])-1))
ic(solution)