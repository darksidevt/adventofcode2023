from icecream import ic
import utils
import re
import itertools
FILE = 'example1.txt'
data = utils.open_file(FILE)
winning_list = []

def get_row(row_number, all_rows):
    return all_rows[row_number]
def get_rows(row_start, row_end, all_rows):
    return all_rows[row_start:row_end]
for line in data:
    reg = r'item \d+:'
    line = re.sub(reg, '', line)
    line = line.replace('  ', ' ')
    line = line.split('|')
    # Line[0] in the list is the winning numbers
    # Line[1] in the list is the numbers on the item
    line[0] = line[0].strip()
    line[0] = line[0].split(' ')
    line[1] = line[1].strip()
    line[1] = line[1].split(' ')
    single_game_list = []
    for winning_number in line[0]:
        if winning_number in line[1]:
            single_game_list.append(winning_number)
    winning_list.append(single_game_list)
ic(winning_list)
num_of_wins_per_game = []
for winning_list_index, winning_numbers in enumerate(winning_list):
    num_of_wins_per_game.append(len(winning_numbers))

'''
Example:
Each index in the list represents the number of wins per game
[4, 2, 2, 1, 0, 0]

item 0 has four matching numbers, so you win one copy each of the next four items: items 1, 2, 3, and 4.
Your original item 1 has two matching numbers, so you win one copy each of items 2 and 3.
Your copy of item 1 also wins one copy each of items 2 and 3.
Your four instances of item 2 (one original and three copies) have two matching numbers, so you win four copies each of items 3 and 4.
Your eight instances of item 3 (one original and seven copies) have one matching number, so you win eight copies of item 4.
Your fourteen instances of item 4 (one original and thirteen copies) have no matching numbers and win no more items.
Your one instance of item 5 (one original) has no matching numbers and wins no more items.

Once all of the originals and copies have been processed, you end up with 1 instance of card 1,
2 instances of card 2, 4 instances of card 3, 8 instances of card 4, 14 instances of card 5, and 1 instance of card 6.
In total, this example pile of scratchcards causes you to ultimately have 30 scratchcards. 
'''
def add_total(data, index):
    ic(data[index]['num_of_wins'])
    for win_iterator in range(index+index, data[index]['num_of_wins']+index+1):
        ic(win_iterator)
    return data
ic(num_of_wins_per_game)
data = {}
for index, num_of_wins in enumerate(num_of_wins_per_game):
    data[index] = {'num_of_wins': num_of_wins, 'num_of_copies': 1}
for i in range(len(num_of_wins_per_game)):
    data = add_total(data, i)
    ic("Here")
    ic(data)
# new_data = [num_of_wins_per_game, [1] * len(num_of_wins_per_game)]

# max_itterations = len(num_of_wins_per_game)
# itterations = 0
# def add_wins_to_total_games(num_of_wins, starting_index, win_list):
#     for i in range(starting_index, starting_index+num_of_wins):
#         ic(i)
#         try:
#             win_list[i] += 1
#         except IndexError:
#             pass
#     return win_list
# # ic(add_wins_to_total_games(3, 2, [1, 1, 1, 1, 1, 1]))

# for index, card_wins in enumerate(num_of_wins_per_game):
#     # ic(card_wins)
#     for numb_number in range(card_wins):
#         print(f"Which card?: {index}")
#         print(f"Adding 1 to index {numb_number + 1}")
#         print(f"Current number at index {new_data[1][numb_number+1]}")
#         new_data[1][numb_number+1] += 1
#         print(f"New number at index {new_data[1][numb_number+1]}")
#         # ic(numb_number)
#         # new_data[1][numb_number] += 1
#         # new_data[1] = add_wins_to_total_games(card_wins, index, new_data[1])
# #     for starting_index, num_of_wins in enumerate(num_of_wins_per_game):
# #         if starting_index <= starting_index+num_of_wins:
# #             new_data[1] = add_wins_to_total_games(num_of_wins, starting_index, new_data[1])
# #     for i in range(len(num_of_wins_per_game)):
# #         if num_of_wins_per_game[i] > 0:
# #             new_data[1][i] += num_of_wins_per_game[i]
# ic(new_data)
# ic(sum(num_of_wins_per_game))