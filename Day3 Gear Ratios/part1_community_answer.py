import re
from icecream import ic
FILE_NAME = 'input.txt'
with open(FILE_NAME, 'r') as f:
    data = f.read().splitlines()
sol = 0
# Itterate over rows
for row in range(len(data)):
    # Find all numbers in the row
    for match in re.finditer('\d+', data[row]):
        try:
            for number in range(*match.span()):
                for left_right in range(-1, 2):
                    for above_below in range(-1, 2):
                        assert not (0 <= row+left_right < len(data) and 0 <= number+above_below < len(data) and not data[row+left_right][number+above_below].isdigit() and data[row+left_right][number+above_below] != '.')
        except: sol += int(match.group())
ic(sol)