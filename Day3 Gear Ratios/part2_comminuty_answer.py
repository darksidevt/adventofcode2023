import re
from icecream import ic
FILE_NAME = 'input.txt'
with open(FILE_NAME, 'r') as f:
    data = f.read().splitlines()
sol = 0
adj = [[[] for x in range(len(data))] for x in range(len(data))]
for x in range(len(data)):
    for match in re.finditer('\d+', data[x]):
        for y in range(*match.span()):
            for i in range(-1, 2):
                for j in range(-1 if y == match.span()[0] else 0, 1 if y < match.span()[1]-1 else 2):
                    if 0 <= x+i < len(data) and 0 <= y+j < len(data) and data[x+i][y+j] == '*':
                        adj[x+i][y+j].append(int(match.group()))
ic(sum(adj[x][y][0]*adj[x][y][1] for x in range(len(data)) for y in range(len(data)) if len(adj[x][y]) == 2))