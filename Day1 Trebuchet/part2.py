def first_number(text):
    temp_string = ''
    for char in text:
        temp_string += char
        if ("0" in temp_string) or ("zero" in temp_string):
            return '0'
        elif ("1" in temp_string) or ("one" in temp_string):
            return '1'
        elif ("2" in temp_string) or ("two" in temp_string):
            return '2'
        elif ("3" in temp_string) or ("three" in temp_string):
            return '3'
        elif ("4" in temp_string) or ("four" in temp_string):
            return '4'
        elif ("5" in temp_string) or ("five" in temp_string):
            return '5'
        elif ("6" in temp_string) or ("six" in temp_string):
            return '6'
        elif ("7" in temp_string) or ("seven" in temp_string):
            return '7'
        elif ("8" in temp_string) or ("eight" in temp_string):
            return '8'
        elif ("9" in temp_string) or ("nine" in temp_string):
            return '9'
        else:
            continue
def last_number(text):
    temp_string = ''
    for char in text[::-1]:
        temp_string += char
        if ("0" in temp_string[::-1]) or ("zero" in temp_string[::-1]):
            return '0'
        elif ("1" in temp_string[::-1]) or ("one" in temp_string[::-1]):
            return '1'
        elif ("2" in temp_string[::-1]) or ("two" in temp_string[::-1]):
            return '2'
        elif ("3" in temp_string[::-1]) or ("three" in temp_string[::-1]):
            return '3'
        elif ("4" in temp_string[::-1]) or ("four" in temp_string[::-1]):
            return '4'
        elif ("5" in temp_string[::-1]) or ("five" in temp_string[::-1]):
            return '5'
        elif ("6" in temp_string[::-1]) or ("six" in temp_string[::-1]):
            return '6'
        elif ("7" in temp_string[::-1]) or ("seven" in temp_string[::-1]):
            return '7'
        elif ("8" in temp_string[::-1]) or ("eight" in temp_string[::-1]):
            return '8'
        elif ("9" in temp_string[::-1]) or ("nine" in temp_string[::-1]):
            return '9'
        else:
            continue
with open('input_original.txt', 'r') as f:
    lines = f.readlines()
lines = [line.rstrip('\n') for line in lines]
digits = []
for line in lines:
    firstNumber = first_number(line)
    lastNumber = last_number(line)
    digit = firstNumber + lastNumber
    digit = int(digit)
    digits.append(digit)
cords = 0
for i in digits:
    cords += i
print(cords)

