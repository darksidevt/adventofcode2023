with open('input_original.txt', 'r') as f:
    lines = f.readlines()
lines = [line.rstrip('\n') for line in lines]
digits = []
for line in lines:
    temp = []
    for char in line:
        if char.isdigit():
            temp.append(char)
    first_number = temp[0]
    last_number = temp[len(temp)-1]
    number = first_number + last_number
    digits.append(int(number))
print(digits)
cords = 0
for i in digits:
    cords += i
print(cords)